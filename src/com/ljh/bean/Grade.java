package com.ljh.bean;

import java.util.List;

/**
 * @author cW
 * @desc junHao_DemoClass
 * @date 2021-07-16 19:21:45
 */
//表名==类名，列名=属性名
public class Grade {
    private Integer gradeId;
    private String gradeName;
    private List<Scholar> scholarList;

    public Grade() {
    }

    public Grade(Integer gradeId, String gradeName, List<Scholar> scholarList) {
        this.gradeId = gradeId;
        this.gradeName = gradeName;
        this.scholarList = scholarList;
    }

    public Integer getGradeId() {
        return gradeId;
    }

    public void setGradeId(Integer gradeId) {
        this.gradeId = gradeId;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public List<Scholar> getScholarList() {
        return scholarList;
    }

    public void setScholarList(List<Scholar> scholarList) {
        this.scholarList = scholarList;
    }

    @Override
    public String toString() {
        return "Grade{" +
                "gradeId=" + gradeId +
                ", gradeName='" + gradeName + '\'' +
                ", scholarList=" + scholarList +
                '}';
    }
}
