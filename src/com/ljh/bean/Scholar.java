package com.ljh.bean;

import java.util.Date;

/**
 * @author cW
 * @desc junHao_DemoClass
 * @date 2021-07-16 19:22:35
 */
public class Scholar {
    private Integer schId;
    private String schName;
    private String schNo;
    private Integer sex;
    private String phone;
    private String email;
    private String registered;
    private String address;
    private String profession;
    private String idNumber;
    private String politics;
    private Date regDate;
    private Integer State;
    private String introduction;
    private Integer gid;
    private Grade grade;

    public Scholar() {
    }

    public Scholar(Integer schId, String schName, String schNo, Integer sex, String phone, String email, String registered, String address, String profession, String idNumber, String politics, Date regDate, Integer state, String introduction, Integer gid, Grade grade) {
        this.schId = schId;
        this.schName = schName;
        this.schNo = schNo;
        this.sex = sex;
        this.phone = phone;
        this.email = email;
        this.registered = registered;
        this.address = address;
        this.profession = profession;
        this.idNumber = idNumber;
        this.politics = politics;
        this.regDate = regDate;
        State = state;
        this.introduction = introduction;
        this.gid = gid;
        this.grade = grade;
    }

    public Integer getSchId() {
        return schId;
    }

    public void setSchId(Integer schId) {
        this.schId = schId;
    }

    public String getSchName() {
        return schName;
    }

    public void setSchName(String schName) {
        this.schName = schName;
    }

    public String getSchNo() {
        return schNo;
    }

    public void setSchNo(String schNo) {
        this.schNo = schNo;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRegistered() {
        return registered;
    }

    public void setRegistered(String registered) {
        this.registered = registered;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getPolitics() {
        return politics;
    }

    public void setPolitics(String politics) {
        this.politics = politics;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public Integer getState() {
        return State;
    }

    public void setState(Integer state) {
        State = state;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public Integer getGid() {
        return gid;
    }

    public void setGid(Integer gid) {
        this.gid = gid;
    }

    public Grade getGrade() {
        return grade;
    }

    public void setGrade(Grade grade) {
        this.grade = grade;
    }

    @Override
    public String toString() {
        return "Scholar{" +
                "schId=" + schId +
                ", schName='" + schName + '\'' +
                ", schNo='" + schNo + '\'' +
                ", sex=" + sex +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", registered='" + registered + '\'' +
                ", address='" + address + '\'' +
                ", profession='" + profession + '\'' +
                ", idNumber='" + idNumber + '\'' +
                ", politics='" + politics + '\'' +
                ", regDate=" + regDate +
                ", State=" + State +
                ", introduction='" + introduction + '\'' +
                ", gid=" + gid +
                ", grade=" + grade +
                '}';
    }
}
