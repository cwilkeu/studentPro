package com.ljh.bean;

/**
 * @author cW
 * @desc junHao_DemoClass
 * @date 2021-07-16 19:21:35
 */
public class Users {
    private Integer userid;
    private String loginName;
    private String password;
    private String realname;

    public Users() {
    }

    public Users(Integer userid, String loginName, String password, String realname) {
        this.userid = userid;
        this.loginName = loginName;
        this.password = password;
        this.realname = realname;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    @Override
    public String toString() {
        return "Users{" +
                "userid=" + userid +
                ", loginName='" + loginName + '\'' +
                ", password='" + password + '\'' +
                ", realname='" + realname + '\'' +
                '}';
    }
}
