package com.ljh.dao;

import com.ljh.bean.Scholar;

import java.util.List;

/**
 * @author cW
 * @desc junHao_DemoClass
 * @date 2021-07-16 19:35:34
 */
public interface ScholarDao {
    /**
     * 获取学员的信息列表
     */
    public List<Scholar> getScholar(String name,String stuno,int sex);
}
