package com.ljh.dao;

import com.ljh.bean.Users;

/**
 * @author cW
 * @desc junHao_DemoClass
 * @date 2021-07-16 19:35:51
 */
public interface UsersDao {
    /**
     * 登录方法
     */
    public Users login(String username, String password);
}
