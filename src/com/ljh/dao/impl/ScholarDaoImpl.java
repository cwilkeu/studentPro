package com.ljh.dao.impl;

import com.ljh.bean.Scholar;
import com.ljh.dao.DBUtils;
import com.ljh.dao.ScholarDao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author cW
 * @desc junHao_DemoClass
 * @date 2021-07-16 19:36:59
 */
public class ScholarDaoImpl extends DBUtils implements ScholarDao {
    @Override
    public List<Scholar> getScholar(String name,String stuno,int sex) {
        List list = new ArrayList<>();
        List params = new ArrayList();
        try {
            StringBuffer sqlbuf = new StringBuffer(" select * from scholar where 1=1 ");
            if (name!=null&&name.length()>0){
                sqlbuf.append(" and stuname like ? ");
                params.add("%"+name+"%");
            }
            if (stuno!=null&&stuno.length()>0){
                sqlbuf.append(" and stuno=? ");
                params.add(stuno);
            }
            if (sex!=-1){
                sqlbuf.append(" and sex=? ");
                params.add(sex);
            }

            resultSet = query(sqlbuf.toString(), params);
            while (resultSet.next()){
                Scholar scholar = new Scholar();
                scholar.setSchId(resultSet.getInt("schId"));
                scholar.setSchNo(resultSet.getString("schNo"));
                scholar.setSchName(resultSet.getString("stuname"));
                scholar.setSex(resultSet.getInt("sex"));
                scholar.setPhone(resultSet.getString("phone"));
                scholar.setProfession(resultSet.getString("profession"));
                scholar.setRegDate(resultSet.getDate("regDate"));
                //补全所有的列
                list.add(scholar);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeAll();
        }
        return null;
    }
}
