package com.ljh.dao.impl;

import com.ljh.bean.Users;
import com.ljh.dao.DBUtils;
import com.ljh.dao.UsersDao;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author cW
 * @desc junHao_DemoClass
 * @date 2021-07-16 19:36:15
 */
public class UsersDaoImpl extends DBUtils implements UsersDao {
    @Override
    public Users login(String username, String password) {
        Users users = null;
        try {
            String sql = "select * from users where loginName=? and password=?";
            ArrayList arrayList = new ArrayList();
            arrayList.add(username);
            arrayList.add(password);
            resultSet = query(sql, arrayList);
            if(resultSet==null){
                return null;
            }
            users = null;
            while (resultSet.next()){
                users=new Users();
                users.setLoginName(username);
                users.setRealname(resultSet.getString("realname"));
                users.setUserid(resultSet.getInt("userid"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeAll();
        }
        return users;
    }
}
