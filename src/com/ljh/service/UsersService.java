package com.ljh.service;

import com.ljh.bean.Users;

import javax.swing.*;

/**
 * @author cW
 * @desc junHao_DemoClass
 * @date 2021-07-16 19:35:51
 */
public interface UsersService {
    /**
     * 登录方法
     */
    public Users login(String username,String password);
}
