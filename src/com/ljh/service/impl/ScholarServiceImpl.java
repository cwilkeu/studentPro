package com.ljh.service.impl;

import com.ljh.bean.Scholar;
import com.ljh.dao.ScholarDao;
import com.ljh.dao.impl.ScholarDaoImpl;
import com.ljh.service.ScholarService;

import java.util.List;

/**
 * @author cW
 * @desc junHao_DemoClass
 * @date 2021-07-16 19:36:59
 */
public class ScholarServiceImpl implements ScholarService {
    private ScholarDao dao = new ScholarDaoImpl();
    @Override
    public List<Scholar> getScholar(String name,String stuno,int sex) {
        return dao.getScholar(name,stuno,sex);
    }
}
