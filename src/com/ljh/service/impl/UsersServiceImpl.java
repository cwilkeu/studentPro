package com.ljh.service.impl;

import com.ljh.bean.Users;
import com.ljh.dao.UsersDao;
import com.ljh.dao.impl.UsersDaoImpl;
import com.ljh.service.UsersService;

/**
 * @author cW
 * @desc junHao_DemoClass
 * @date 2021-07-16 19:36:15
 */
public class UsersServiceImpl implements UsersService {
    private UsersDao usersDao = new UsersDaoImpl();
    @Override
    public Users login(String username, String password) {
        return usersDao.login(username,password);
    }
}
