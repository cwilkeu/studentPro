package com.ljh.web;

import com.ljh.bean.Scholar;
import com.ljh.service.ScholarService;
import com.ljh.service.impl.ScholarServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author cW
 * @desc junHao_DemoClass
 * @date 2021-07-17 17:51:04
 */
@WebServlet(urlPatterns = "/Educational/student/getScholarList")
public class GetScholarServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1.获取参数
        String stuname = req.getParameter("stuname");
        String stuno = req.getParameter("stuno");
        String sex = req.getParameter("sex");
        //2.读取service方法
        ScholarService service = new ScholarServiceImpl();
        List<Scholar> scholars = service.getScholar(stuname,stuno,Integer.parseInt(sex));

        //3.跳转页面
        //如果后台想给前台传数据，是一定要在后台存值的
        req.setAttribute("stulist",scholars);
        req.getRequestDispatcher("list.jsp").forward(req,resp);
    }
}
